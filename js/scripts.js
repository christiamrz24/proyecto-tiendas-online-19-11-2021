function pintar(id){
    if (id == 1){
        $("#p-1").addClass("texto-filtro-bg"); //Agregamos la clase
        $("#p-2, #p-3, #p-4, #p-5, #p-6, #p-7").removeClass("texto-filtro-bg"); //Quitamos las demas clases
    }
    if (id == 2){
        $("#p-2").addClass("texto-filtro-bg"); //Agregamos la clase
        $("#p-1, #p-3, #p-4, #p-5, #p-6, #p-7").removeClass("texto-filtro-bg"); //Quitamos las demas clases
    }
    if (id == 3){
        $("#p-3").addClass("texto-filtro-bg"); //Agregamos la clase
        $("#p-2, #p-1, #p-4, #p-5, #p-6, #p-7").removeClass("texto-filtro-bg"); //Quitamos las demas clases
    }
    if (id == 4){
        $("#p-4").addClass("texto-filtro-bg"); //Agregamos la clase
        $("#p-2, #p-3, #p-1, #p-5, #p-6, #p-7").removeClass("texto-filtro-bg"); //Quitamos las demas clases
    }
    if (id == 5){
        $("#p-5").addClass("texto-filtro-bg"); //Agregamos la clase
        $("#p-2, #p-3, #p-4, #p-1, #p-6, #p-7").removeClass("texto-filtro-bg"); //Quitamos las demas clases
    }
    if (id == 6){
        $("#p-6").addClass("texto-filtro-bg"); //Agregamos la clase
        $("#p-2, #p-3, #p-4, #p-5, #p-1, #p-7").removeClass("texto-filtro-bg"); //Quitamos las demas clases
    }
    if (id == 7){
        $("#p-7").addClass("texto-filtro-bg"); //Agregamos la clase
        $("#p-2, #p-3, #p-4, #p-5, #p-6, #p-1").removeClass("texto-filtro-bg"); //Quitamos las demas clases
    }
}
function go_home(){
    location.href = "./index.html";
}
function info(){
    location.href = "./info_product.html";
}
function proc_pago(){
    location.href = "./payment.html";
}